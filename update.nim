import os
import osproc
import posix_utils
import strformat
import strutils

import therapist

template withTempDir(prefix: string, code: untyped): untyped =
    let tempdirname {.inject.} = absolutePath(mkdtemp(prefix))
    try:
        code
    finally:
        os.removeDir(tempdirname)

template `&&`*(command1: untyped, command2: untyped): int =
    let status: int = command1
    if status!=0:
        return status
    command2

template withDir(dir: string, code: untyped) = 
    let cwd = os.getCurrentDir()
    try:
        os.setCurrentDir(dir)
        code
    finally:
        os.setCurrentDir(cwd)

proc run(commands: varargs[string]): int = execShellCmd(quoteShellCommand(commands))
proc runex(commands: varargs[string]): tuple[output: TaintedString, exitCode: int, ] = 
    let command = quoteShellCommand(commands)
    result = execCmdEx(command, {poUsePath})
    if result.exitCode!=0:
        echo fmt"Failed> {command}"

proc build(project: string): int =
    let base = getCurrentDir()
    withTempDir "build":
        withDir tempdirname:
            result = (run ["git", "clone", fmt"https://bitbucket.org/maxgrenderjones/{project}/", project])
            if result!=0:
                return
            withDir project:
                result = (run ["nimble", "docs"]) 
                if result!=0: 
                    return
                removeDir(fmt"{base}/{project}/latest")
                copyDir("build/docs", fmt"{base}/{project}/latest")
                let (tags, status) = runex("git", "tag", "--list")
                if status!=0:
                    return status
                for tag in tags.splitLines:
                    if len(tag)==0:
                        continue
                    result = (run ["git", "checkout", tag]) && (run ["nimble", "docs"]) 
                    if result!=0:
                        return
                    removeDir(fmt"{base}/{project}/{tag}")
                    copyDir("build/docs", fmt"{base}/{project}/{tag}")
                



if isMainModule:
    let spec = (
        project: newStringArg("<project>", help="Project(s) to build", multi=true)
    )
    spec.parseOrQuit()
    var status: int
    for project in spec.project.values:
        status = build(project)
        if status!=0:
            quit(status)