import htmlgen

import therapist

if isMainModule:

    let spec = (
        file: newStringArg("<filename>", help="File to write to", defaultVal="index.html", optional=true),
        help: newHelpArg()
    )

    spec.parseOrQuit()

    let output =
        html(
            head(
                link(href="https://fonts.googleapis.com/css2?family=Oxygen&family=Open+Sans&&display=swap", rel="stylesheet"),
                style("""
                h3 {
                    font-family: Oxygen, Helvetica, sans-serif
                }
                body {
                    font-family: 'Open Sans', Helvetica, sans-serif
                }
                """)
            ),
                body(
                h3("Therapist"),
                p(
                    ul(
                        li("latest",
                            ul(
                                li(a(href="therapist/latest/README.html", "README")),
                                li(a(href="therapist/latest/therapist.html", "therapist")),
                            )
                        ),
                        li("v0.2.0",
                            ul(
                                li(a(href="therapist/v0.2.0/README.html", "README")),
                                li(a(href="therapist/v0.2.0/therapist.html", "therapist")),
                            )
                        ),
                        li("v0.1.0",
                            ul(
                                li(a(href="therapist/v0.1.0/README.html", "README")),
                                li(a(href="therapist/v0.1.0/therapist.html", "therapist")),
                            )
                        )
                    )
                ),
                h3("Argparse"),
                p(
                    ul(
                        li("latest",
                            ul(
                                li(a(href="argparse/latest/argparse.html", "argparse")),
                            )
                        ),
                    )
                ),
                h3("bitlines"),
                p(
                    ul(
                        li("latest",
                            ul(
                                li(a(href="bitlines/latest/README.html", "README")),
                            )
                        ),
                    )
                ),

                h3("grammar"),
                p(
                    ul(
                        li("latest",
                            ul(
                                li(a(href="grammar/latest/grammar.html", "grammar")),
                            )
                        ),
                    )
                )
 
            )
        )
    writeFile(spec.file.value, output)
